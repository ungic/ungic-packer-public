module.exports = {
	"supportedTypes": {
		"txt": "text",
		"hbs": "template",
		"mustache": "mustache_template",
		"_": "underscore_template",
		"pug": "pug_template",
		"html": "part",
		"md": "markdown",
		"json": "data",
		"yaml": "data"
	},
	"supportedIncludeTypes": [
	  "txt",
	  "hbs",
	  "html",
	  "md",
	  "mustache",
	  "pug",
	  "_"
	]
}