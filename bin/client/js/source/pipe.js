import JSONTreeView from "json-tree-view";
import io from 'socket.io-client';

document.addEventListener("DOMContentLoaded", function() {
    let debugs = document.querySelectorAll('.un-debug');
    if(debugs.length) {
        for(let debug of debugs) {
            let text = debug.innerHTML;
            try {
                let data = JSON.parse(text);
                let path = debug.getAttribute('data-path');
                if(!path) {
                    path = 'debug'
                }
                let view = new JSONTreeView(path, data);
                debug.style.display = 'none';
                let wrap = document.createElement('div');
                wrap.setAttribute('class', 'jsonViewWrap');
                wrap.appendChild(view.dom);
                debug.parentNode.insertBefore(wrap, debug);
                view.expand(true);
                view.readonly = true;
            } catch(e) {
                console.log(e);
            }
        }
    }

    let connect = document.querySelector('[data-connect]').getAttribute('data-connect');
    const socket = io(connect);
    const resource = window.performance ? window.performance.getEntriesByType("resource") : [];
    const pagesrc = document.querySelector('[data-connect]').getAttribute('data-src');
    socket.on('change', (events) => {
        for(let e of events) {
            let {events, relative, url}  = e;
            if(relative == pagesrc) {
                window.location.reload();
                return
            }
            let skips = [];
            if(e.relative.indexOf('.css') != -1) {
                let links = document.querySelectorAll('[href*="'+relative+'"]');
                if(links.length) {
                    for(let link of links) {
                        link.setAttribute('href', url + '?v=' + Date.now());
                        skips.push(link);
                    }
                }
            }
            for(let res of resource) {
                if(res.name.indexOf(url) != -1) {
                    if(res.initiatorType == 'link') {
                        let links = document.querySelectorAll('[href*="'+relative+'"]');
                        if(links.length) {
                            for(let link of links) {
                                if(skips.indexOf(link) == -1) {
                                    link.setAttribute('href', url + '?v=' + Date.now());
                                }
                            }
                        }
                    } else {
                        window.location.reload();
                    }
                }
            }
        }
    });
});